# gttk

Gttk is my fork of the tile-gtk project that gives to your Tk applications a native [GTK+](http://www.gtk.org/) look and feel.

![Screenshot image here]()

## Installation
```
cmake -DCMAKE_INSTALL_PREFIX=`tclsh <<< 'puts [lindex $auto_path end]'`
make
sudo make install
```

## Todo for me
- rename gtkTtk to gttk
- install some GNOME desktop and make a screenshot
- progressbar fix
- combobox fix
